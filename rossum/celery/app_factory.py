# pylint: disable=syntax-error,no-name-in-module, unused-argument
from typing import Any

from celery import Celery, Task, current_app, signals
from flask import Flask

from ..database.models import rossum_db
from ..settings import REDIS_BROKER_URL

QUEUE_NAME = "rossum.document_conversions"


# make sure celery won't make any changes to logging config
@signals.setup_logging.connect
def setup_celery_logging(**kwargs: Any) -> None:
    pass


@signals.task_postrun.connect
def dispose_db_connections(**kwargs: Any) -> None:
    with current_app.flask_app.app_context():
        rossum_db.session.remove()
        rossum_db.session.bind.pool.dispose()


class ContextTask(Task):
    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        with self.app.flask_app.app_context():
            return super().__call__(*args, **kwargs)


class ContextedCelery(Celery):
    task_cls = ContextTask

    def __init__(self, flask_app: Flask, *args: Any, **kwargs: Any):
        self.flask_app = flask_app
        super().__init__(*args, **kwargs)


def make_celery_app(flask_app: Flask) -> ContextedCelery:
    """Create Celery application and make all tasks to be executed in a Flask context."""
    celery = ContextedCelery(flask_app, "rossum", broker=REDIS_BROKER_URL, include=["rossum.celery.tasks"])
    celery.conf.accept_content = ["json"]
    celery.conf.task_default_queue = QUEUE_NAME

    return celery
