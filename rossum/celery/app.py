from ..application.app import create_connexion_app
from .app_factory import make_celery_app

# Used only as an entrypoint. Do not import directly, use `celery.current_app`
celery = make_celery_app(create_connexion_app())
