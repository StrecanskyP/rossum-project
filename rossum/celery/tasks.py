import structlog
from flask import current_app as flask_current_app

from ..converter import Converter
from .app_factory import make_celery_app

logger = structlog.get_logger()
celery_app = make_celery_app(flask_current_app)


@celery_app.task(soft_time_limit=60 * 10)
def process_document(document_id: int) -> None:
    """Convert PDF document into normalised PNG pages."""
    logger.info("task_received", document_id=document_id)
    converter = Converter(document_id=document_id)
    converter.run()
