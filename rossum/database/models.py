from __future__ import annotations

from enum import Enum
from typing import List

from flask_sqlalchemy import SQLAlchemy

rossum_db = SQLAlchemy()


class DocumentStatus(Enum):
    processing = "processing"
    done = "done"


class Document(rossum_db.Model):  # type: ignore
    __tablename__ = "documents"

    id = rossum_db.Column(rossum_db.Integer, primary_key=True)
    status = rossum_db.Column(rossum_db.Enum(DocumentStatus), nullable=False)
    n_pages = rossum_db.Column(rossum_db.Integer, nullable=False)
    content = rossum_db.Column(rossum_db.LargeBinary, nullable=False)
    inserted_at = rossum_db.Column(rossum_db.DateTime, server_default=rossum_db.func.now(), nullable=False)
    updated_at = rossum_db.Column(
        rossum_db.DateTime, server_default=rossum_db.func.now(), onupdate=rossum_db.func.now(), nullable=False,
    )

    pages: List[NormalisedPage] = rossum_db.relationship("NormalisedPage", backref="document")


class NormalisedPage(rossum_db.Model):  # type: ignore
    __tablename__ = "normalised_pages"

    id = rossum_db.Column(rossum_db.Integer, primary_key=True)
    document_id = rossum_db.Column(rossum_db.Integer, rossum_db.ForeignKey("documents.id"), nullable=False)
    page = rossum_db.Column(rossum_db.Integer, nullable=False)
    content = rossum_db.Column(rossum_db.LargeBinary, nullable=False)
    inserted_at = rossum_db.Column(rossum_db.DateTime, server_default=rossum_db.func.now(), nullable=False)
    updated_at = rossum_db.Column(
        rossum_db.DateTime, server_default=rossum_db.func.now(), onupdate=rossum_db.func.now(), nullable=False,
    )
