from typing import Any

import structlog
from sqlalchemy.exc import SQLAlchemyError

from ..models import rossum_db as db

logger = structlog.get_logger()


class SQLAlchemyRepository:
    def __init__(self) -> None:
        self.session = db.session

    def add(self, model: Any) -> None:
        self.session.add(model)

    def commit(self) -> None:
        try:
            self.session.commit()
        except SQLAlchemyError:
            logger.exception()
            self.session.rollback()
            raise
