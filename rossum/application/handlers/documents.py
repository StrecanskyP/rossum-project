from typing import Tuple

from flask import Response, jsonify, send_file

from rossum.celery.tasks import process_document
from rossum.documents import load_document, load_document_page, upload_document
from rossum.exceptions import DocumentNotFoundError, InvalidPdfError, NormalisedPageNotFoundError


def post(body: bytes) -> Tuple[Response, int]:
    """Endpoint for uploading PDF files."""
    try:
        document_id = upload_document(body)
        process_document.delay(document_id)
    except InvalidPdfError:
        return jsonify({"status": "error", "message": "Invalid PDF file"}), 400
    return jsonify({"id": document_id}), 201


def get(document_id: int) -> Tuple[Response, int]:
    """Endpoint for loading processing state of a document."""
    try:
        document_details = load_document(document_id=document_id)
        return jsonify(document_details), 200
    except DocumentNotFoundError:
        return jsonify({"status": "error", "message": "Document not found"}), 404


def get_page(document_id: int, number: int) -> Tuple[Response, int]:
    """Endpoint for rendering normalised page of a document."""
    try:
        page = load_document_page(document_id=document_id, page_no=number)
        return send_file(page, mimetype="image/png"), 200
    except NormalisedPageNotFoundError:
        return jsonify({"status": "error", "message": "Page not found"}), 404
