# pylint: disable=protected-access
import os

import PIL
import pytest

from rossum.converter import Converter
from rossum.database.models import Document, DocumentStatus, NormalisedPage

script_path = os.path.dirname(__file__)
test_file_path = os.path.join(script_path, "../test_files")


def test_process_page(database, document_factory):
    """Test whether single page is correctly saved under corresponding PDF document."""
    doc = document_factory(n_pages=1)
    random_image = PIL.Image.open(fp=f"{test_file_path}/rossum_logo.jpg")

    Converter(doc.id)._process_page(page_number=1, converted_image=random_image)

    assert database.session.query(NormalisedPage).filter_by(document_id=doc.id, page=1).one()


@pytest.mark.parametrize("file_name, file_pages", [("test_cv.pdf", 1), ("test_invoice.pdf", 10)])
def test_run(database, file_name, file_pages, document_factory):
    """Test the whole conversion process."""
    doc = document_factory(file_name=file_name, n_pages=file_pages)
    Converter(document_id=doc.id).run()

    document = database.session.query(Document).filter_by(id=doc.id).one()
    assert len(document.pages) == document.n_pages
    assert document.status == DocumentStatus.done
