# pylint: disable=unused-argument


def test_ping(client, database):
    response = client.get("/ping")
    assert response.status_code == 200
    assert response.json == {"status": "ok", "message": "Pong"}
