# pylint: disable=redefined-outer-name, wrong-import-position)
import pytest
from pytest_factoryboy import register

from rossum.application.app import create_connexion_app
from rossum.database.models import rossum_db

from .factories.document import DocumentFactory, NormalisedPageFactory

register(DocumentFactory)
register(NormalisedPageFactory)


@pytest.fixture(scope="session")
def connexion_app():
    yield create_connexion_app()


@pytest.fixture(scope="session")
def app(connexion_app):
    """Application instance for the tests."""
    app = connexion_app

    with app.app_context():
        yield app


def db_guard():
    """Check if the tests are not trying to delete production DB."""
    existing_tables = rossum_db.engine.table_names()
    if existing_tables and existing_tables != ["alembic_version"]:
        pytest.exit(
            f"The test database is not empty, GET OUT before you drop all production data. "
            f"Tables: {existing_tables}"
        )


@pytest.fixture(scope="session")
def database(app):
    # db_guard()
    rossum_db.create_all()
    yield rossum_db
    rossum_db.session.close()
    rossum_db.drop_all(app=app)
